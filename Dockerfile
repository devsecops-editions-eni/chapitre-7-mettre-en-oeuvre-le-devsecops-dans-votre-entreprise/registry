FROM debian:12.2
RUN apt-get update && apt-get install -y --no-install-recommends \ 
    curl=7.88.1-10* \ 
    nginx=1.22.1-9* \
    git=1:2.39.5-0* \
    sudo=1.9.13p3-1* \
    && apt-get clean \
    && rm -rf /var/lib/apt/lists/* \
    && addgroup user \
    && useradd -rm -d /home/user -s /bin/bash -g user -u 1001 user \
    && echo "user ALL=(ALL) NOPASSWD: /usr/sbin/service nginx start" >> /etc/sudoers.d/user

USER user

CMD ["sleep", "infinity"]